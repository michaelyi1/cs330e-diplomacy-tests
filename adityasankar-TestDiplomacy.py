#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  "A")
        self.assertEqual(city, "Madrid")
        self.assertEqual(action, ["Hold"])

    def test_read_2(self):
        s = "B London Support A\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  "B")
        self.assertEqual(city, "London")
        self.assertEqual(action, ["Support", "A"])

    def test_read_3(self):
        s = "C Austin Move London\n"
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  "C")
        self.assertEqual(city, "Austin")
        self.assertEqual(action, ["Move", "London"])
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", ["Hold"]], ["B", "London", ["Support", "A"]],["C", "Austin", ["Move", "London"]],["D", "Beijing", ["Move", "Madrid"]],["E", "Dallas", ["Support", "B"]]])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'London'], ['C', '[dead]'], ['D', '[dead]'], ['E', 'Dallas']])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", ["Hold"]], ["B", "London", ["Support", "B"]],["C", "Austin", ["Move", "London"]],["D", "Beijing", ["Move", "Madrid"]],["E", "Dallas", ["Support", "B"]]])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'London'], ['C', '[dead]'], ['D', '[dead]'], ['E', 'Dallas']])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Edison", ["Hold"]], ["B", "London", ["Hold"]],["C", "Austin", ["Support", "A"]],["D", "Beijing", ["Move", "Edison"]],["E", "Dallas", ["Move", "Austin"]]])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'London'], ['C', '[dead]'], ['D', '[dead]'], ['E', '[dead]']])

    def test_eval_4(self):
        v = diplomacy_eval([["A", "Dallas", ["Move", "Austin"]], ["B", "Barcelona", ["Move", "Beijing"]],["C", "London", ["Support", "B"]], ["D", "Chennai", ["Move", "Madrid"]], ["E", "Austin", ["Move", "London"]]])
        self.assertEqual(v, [['A', 'Austin'], ['B', 'Beijing'], ['C', '[dead]'], ['D', 'Madrid'], ['E', '[dead]']])

    def test_eval_5(self):
        v = diplomacy_eval([["A", "Dallas", ["Support", "E"]], ["B", "Barcelona", ["Move", "London"]],["C", "Houston", ["Support", "B"]], ["D", "Chennai", ["Move", "Madrid"]], ["E", "Austin", ["Move", "London"]]])
        self.assertEqual(v, [['A', 'Dallas'], ['B', '[dead]'], ['C', 'Houston'], ['D', 'Madrid'], ['E', '[dead]']])

    def test_eval_6(self):
        v = diplomacy_eval([["A", "Barcelona", ["Hold"]], ["B", "Madrid", ["Support", "D"]],["C", "Beijing", ["Hold"]], ["D", "London", ["Move", "Beijing"]]])
        self.assertEqual(v, [['A', 'Barcelona'], ['B', 'Madrid'], ['C', '[dead]'],['D', 'Beijing']])
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'], ['B', 'London'], ['C', '[dead]'], ['D', '[dead]'], ['E', 'Dallas']])
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\nD [dead]\nE Dallas\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'SanDiego'], ['B', '[dead]'], ['C', 'LasVegas'], ['D', 'SanFransisco'], ['E', '[dead]']])
        self.assertEqual(w.getvalue(), "A SanDiego\nB [dead]\nC LasVegas\nD SanFransisco\nE [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Delhi'], ['B', '[dead]'], ['C', 'NewYork']])
        self.assertEqual(w.getvalue(), "A Delhi\nB [dead]\nC NewYork\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Austin Move London\nD Beijing Move Madrid\nE Dallas Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB London\nC [dead]\nD [dead]\nE Dallas\n")

    def test_solve_2(self):
        r = StringIO("A AtlanticCity Move Chicago\nB Boston Support G\nC Cabo Support A\nD Ithaca Move Chicago\nE ElPaso Support D\nF FortWorth Move Chicago\nG Chicago Move Houston\nH Houston Move AtlanticCity\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Boston\nC Cabo\nD [dead]\nE ElPaso\nF [dead]\nG Houston\nH AtlanticCity\n")

    def test_solve_3(self):
        r = StringIO(
            "A Austin Support B\nB SanAntonio Move Naples\nC Cairo Support D\nD Naples Move Plainfield\nE Plainfield Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Naples\nC Cairo\nD Plainfield\nE [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
